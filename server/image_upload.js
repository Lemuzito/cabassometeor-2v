/**
 * Created by alejandrolemusrodriguez on 23/01/17.
 */
req = Npm.require('request');
import {Configuration} from '../imports/api/configuration/configuration.js';
Meteor.methods({
    uploadImageCategory:function(data){
        try {
            var response;
            data.token = Configuration.findOne().token;
            return response = req.post(Configuration.findOne().urlStaticServer+'upload/image-category').form(data);
        }catch(e) {
            throw new Meteor.Error('error', 'server problem print');
        }
    },
    uploadImageProduct:function(data){
        try {
            var response;
            data.token = Configuration.findOne().token;
            return response = req.post(Configuration.findOne().urlStaticServer+'upload/image-product').form(data);
        }catch(e) {
            throw new Meteor.Error('error', 'server problem print');
        }
    },
    uploadImageFinger:function(data){
        try {
            var response;
            data.token = Configuration.findOne().token;
            return response = req.post(Configuration.findOne().urlStaticServer+'upload/image-finger').form(data);
        }catch(e) {
            throw new Meteor.Error('error', 'server problem print');
        }
    },
    uploadImagePrincipal:function (data) {
        try {
            var response;
            data.token = Configuration.findOne().token;
            return response = req.post(Configuration.findOne().urlStaticServer+'upload/image-principal').form(data);
        }catch(e) {
            throw new Meteor.Error('error', 'server problem print');
        }
    },
    uploadImageSecondary:function (data) {
        try {
            var response;
            data.token = Configuration.findOne().token;
            return response = req.post(Configuration.findOne().urlStaticServer+'upload/image-secondary').form(data);
        }catch(e) {
            throw new Meteor.Error('error', 'server problem print');
        }
    }
});
