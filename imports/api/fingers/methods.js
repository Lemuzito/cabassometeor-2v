/**
 * Created by alejandrolemusrodriguez on 24/01/17.
 */
import { Fingers } from './fingers.js';

import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { _ } from 'meteor/underscore';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';


export const insert = new ValidatedMethod({
    name: 'Fingers.insert',
    validate: Fingers.simpleSchema().pick(['name']).validator({clean: true, filter: false}),
    run({ name }){
        const finger = {
            name,
            status: false,
            createAt: new Date(),
        };
        Fingers.insert(finger);
    }
});

export const update = new ValidatedMethod({
    name: 'Fingers.update',
    validate: new SimpleSchema({
        _id : Fingers.simpleSchema().schema('_id'),
        name: Fingers.simpleSchema().schema('name'),
    }).validator({clean: true, filter: false}),
    run({_id, name}){
        Fingers.update(_id,{$set:{name:(_.isUndefined(name)? null: name)}});
    },
});

export const updateStatus = new ValidatedMethod({
    name: 'Fingers.updateStatus',
    validate: new SimpleSchema({
        _id: Fingers.simpleSchema().schema('_id'),
        status: Fingers.simpleSchema().schema('status'),
    }).validator({clean:true, filter: false}),
    run({_id,status}){
        Fingers.update(_id,{$set:{status:status}});
    },
});

export const remove = new ValidatedMethod({
    name: 'Fingers.remove',
    validate: new SimpleSchema({
        _id: Fingers.simpleSchema().schema('_id'),
    }).validator({clean: true, filter: false}),
    run({_id}){
        Fingers.remove(_id);
    }
});

const FINGERS_METHODS = _.pluck([
    insert,
    update,
    remove,
    updateStatus,
],'name');

if (Meteor.isServer) {
    DDPRateLimiter.addRule({
        name(name) {
            return _.contains(FINGERS_METHODS, name);
        },
        // Rate limit per connection ID
        connectionId() { return true; },
    }, 5, 1000);
}