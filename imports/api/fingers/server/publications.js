/**
 * Created by alejandrolemusrodriguez on 24/01/17.
 */
import { Fingers } from '../fingers.js';
import { Meteor } from 'meteor/meteor';

Meteor.publish('fingers', function fingersPublic() {
    return Fingers.find();
});