/**
 * Created by alejandrolemusrodriguez on 24/01/17.
 */
import {Mongo} from 'meteor/mongo';
import {SimpleSchema} from 'meteor/aldeed:simple-schema';


class FingersCollection extends Mongo.Collection {

    insert(doc, callback) {
        const ourDoc = doc;
        ourDoc.status = true;
        ourDoc.createdAt = ourDoc.createdAt || new Date();
        return super.insert(ourDoc, callback);
    }

    update(selector, modifier) {
        return super.update(selector, modifier);
    }

    remove(selector) {
        return super.remove(selector);
    }

}

export const Fingers = new FingersCollection('fingers');

Fingers.deny({
    insert() {
        return true;
    },
    update() {
        return true;
    },
    remove() {
        return true;
    },
});

Fingers.schema = new SimpleSchema({
    _id: {type: String, regEx: SimpleSchema.RegEx.Id},
    name: {type: String},
    status: {type: Boolean, defaultValue: false},
    createdAt: {type: Date},
});

Fingers.attachSchema(Fingers.schema);

Fingers.publicFields = {
    name: 1,
    status: 1,
    createdAt: 1,
};