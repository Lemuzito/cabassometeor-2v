/**
 * Created by alejandrolemusrodriguez on 24/01/17.
 */
import {Mongo} from 'meteor/mongo';
import {SimpleSchema} from 'meteor/aldeed:simple-schema';


class LinesCollection extends Mongo.Collection {

    insert(doc, callback) {
        const ourDoc = doc;
        ourDoc.status = true;
        ourDoc.createdAt = ourDoc.createdAt || new Date();
        return super.insert(ourDoc, callback);
    }

    update(selector, modifier) {
        return super.update(selector, modifier);
    }

    remove(selector) {
        return super.remove(selector);
    }

}

export const Lines = new LinesCollection('lines');

Lines.deny({
    insert() {
        return true;
    },
    update() {
        return true;
    },
    remove() {
        return true;
    },
});

Lines.schema = new SimpleSchema({
    _id: {type: String, regEx: SimpleSchema.RegEx.Id},
    name: {type: String},
    supplier: {type: String},
    status: {type: Boolean, defaultValue: false},
    createdAt: {type: Date},
});

Lines.attachSchema(Lines.schema);

Lines.publicFields = {
    name: 1,
    supplier: 1,
    status: 1,
    createdAt: 1,
};