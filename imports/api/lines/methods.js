/**
 * Created by alejandrolemusrodriguez on 24/01/17.
 */
import { Lines } from './lines.js';

import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { _ } from 'meteor/underscore';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';


export const insert = new ValidatedMethod({
    name: 'Lines.insert',
    validate: Lines.simpleSchema().pick(['name','supplier']).validator({clean: true, filter: false}),
    run({ name, supplier}){
        const lines = {
            name,
            supplier,
            status: false,
            createAt: new Date(),
        };
        Lines.insert(lines);
    }
});

export const update = new ValidatedMethod({
    name: 'Lines.update',
    validate: new SimpleSchema({
        _id : Lines.simpleSchema().schema('_id'),
        name: Lines.simpleSchema().schema('name'),
    }).validator({clean: true, filter: false}),
    run({_id, name}){
        Lines.update(_id,{$set:{name:(_.isUndefined(name)? null: name)}});
    },
});

export const updateStatus = new ValidatedMethod({
    name: 'Lines.updateStatus',
    validate: new SimpleSchema({
        _id: Lines.simpleSchema().schema('_id'),
        status: Lines.simpleSchema().schema('status'),
    }).validator({clean:true, filter: false}),
    run({_id,status}){
        Lines.update(_id,{$set:{status:status}});
    },
});

export const remove = new ValidatedMethod({
    name: 'Lines.remove',
    validate: new SimpleSchema({
        _id: Lines.simpleSchema().schema('_id'),
    }).validator({clean: true, filter: false}),
    run({_id}){
        Lines.remove(_id);
    }
});

const LINES_METHODS = _.pluck([
    insert,
    update,
    remove,
    updateStatus,
],'name');

if (Meteor.isServer) {
    DDPRateLimiter.addRule({
        name(name) {
            return _.contains(LINES_METHODS, name);
        },
        // Rate limit per connection ID
        connectionId() { return true; },
    }, 5, 1000);
}