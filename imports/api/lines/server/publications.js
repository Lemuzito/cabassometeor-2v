/**
 * Created by alejandrolemusrodriguez on 24/01/17.
 */
import { Lines } from '../lines.js';
import { Meteor } from 'meteor/meteor';

Meteor.publish('lines', function linesPublic() {
    return Lines.find();
});