/**
 * Created by alejandrolemusrodriguez on 24/01/17.
 */
import { Suppliers } from '../suppliers.js';
import { Meteor } from 'meteor/meteor';

Meteor.publish('suppliers', function suppliersPublic() {
    return Suppliers.find();
});