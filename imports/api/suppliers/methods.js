/**
 * Created by alejandrolemusrodriguez on 24/01/17.
 */
import { Suppliers } from './suppliers.js';

import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { _ } from 'meteor/underscore';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';


export const insert = new ValidatedMethod({
    name: 'Suppliers.insert',
    validate: Suppliers.simpleSchema().pick('name').validator({clean: true, filter: false}),
    run({ name }){
        const subcategory = {
            name,
            status: false,
            createAt: new Date(),
        };
        Suppliers.insert(subcategory);
    }
});

export const update = new ValidatedMethod({
    name: 'Suppliers.update',
    validate: new SimpleSchema({
        _id : Suppliers.simpleSchema().schema('_id'),
        name: Suppliers.simpleSchema().schema('name'),
    }).validator({clean: true, filter: false}),
    run({_id, name}){
        Suppliers.update(_id,{$set:{name:(_.isUndefined(name)? null: name)}});
    },
});

export const updateStatus = new ValidatedMethod({
    name: 'Suppliers.updateStatus',
    validate: new SimpleSchema({
        _id: Suppliers.simpleSchema().schema('_id'),
        status: Suppliers.simpleSchema().schema('status'),
    }).validator({clean:true, filter: false}),
    run({_id,status}){
        Suppliers.update(_id,{$set:{status:status}});
    },
});

export const remove = new ValidatedMethod({
    name: 'Suppliers.remove',
    validate: new SimpleSchema({
        _id: Suppliers.simpleSchema().schema('_id'),
    }).validator({clean: true, filter: false}),
    run({_id}){
        Suppliers.remove(_id);
    }
});

const SUPPLIERS_METHODS = _.pluck([
    insert,
    update,
    remove,
    updateStatus,
],'name');

if (Meteor.isServer) {
    DDPRateLimiter.addRule({
        name(name) {
            return _.contains(SUPPLIERS_METHODS, name);
        },
        // Rate limit per connection ID
        connectionId() { return true; },
    }, 5, 1000);
}