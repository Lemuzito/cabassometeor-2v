/**
 * Created by alejandrolemusrodriguez on 20/01/17.
 */
import { Meteor } from 'meteor/meteor';
import {Accounts} from 'meteor/accounts-base';
// methods
Meteor.methods({
    'users.insert'(userId, doc){
        const password = doc.password;
        doc.profile.status = true;
        delete doc.password;
        // create user
        const user = Meteor.users.insert(doc);
        // set password
        Accounts.setPassword(user, password);
        // response
        throw new Meteor.Error('ok', user);
    },
    'users.update'(userId, id, doc){
        return Meteor.users.update({_id: id}, {$set: {username: doc.username, email: doc.email, 'profile.name': doc.profile.name}});
    },
    'users.delete'(userId, id){
        return Meteor.users.remove({_id: id});
    },
    'users.disable'(userId, id){
        return Meteor.users.update({_id: id}, {$set: {'profile.status': false}});
    },
    'users.enable'(userId, id){
        return Meteor.users.update({_id: id}, {$set: {'profile.status': true}});
    }
});

