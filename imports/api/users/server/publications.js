/**
 * Created by alejandrolemusrodriguez on 20/01/17.
 */
import { Meteor } from 'meteor/meteor';
// publish
if(Meteor.isServer){
    Meteor.publish('users', function usersPublication() {
        return Meteor.users.find();
    });
}