// Register your apis here
//Users
import '../../api/users/methods.js';
import '../../api/users/server/publications.js';
import '../../api/categories/methods.js';
import '../../api/categories/server/publications.js';
import '../../api/sub_categories/methods.js';
import '../../api/sub_categories/server/publications.js';
import '../../api/suppliers/methods.js';
import '../../api/suppliers/server/publications.js';
import '../../api/lines/methods.js';
import '../../api/lines/server/publications.js';
import '../../api/products/methods.js';
import '../../api/products/server/publications.js';
import '../../api/div_sub_categories/methods.js';
import '../../api/div_sub_categories/server/publications.js';
import '../../api/fingers/methods.js';
import '../../api/fingers/server/publications.js';
import '../../api/configuration/methods.js';
import '../../api/configuration/server/publications.js';



