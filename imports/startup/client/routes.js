import {FlowRouter} from 'meteor/kadira:flow-router';
import {BlazeLayout} from 'meteor/kadira:blaze-layout';

/*Import needed templates*/
//Layouts
import '../../ui/layouts/not-found/not-found.js';
import '../../ui/layouts/login/login.js';
import '../../ui/layouts/admin/admin.js';
//Pages
import '../../ui/pages/not-found/not-found.js';
import '../../ui/pages/login/main.js';
import '../../ui/pages/admin/main.js';
import '../../ui/pages/users/main.js';
import '../../ui/pages/categories/main.js';
import '../../ui/pages/sub_categories/main.js';
import '../../ui/pages/lines/main.js';
import '../../ui/pages/suppliers/main.js';
import '../../ui/pages/products/main.js';
import '../../ui/pages/configuration/main.js';
import '../../ui/pages/fingers/main.js';
import '../../ui/pages/div_sub_categories/main.js';

// Set up all routes in the app
FlowRouter.notFound = {
    action() {
        BlazeLayout.render('notFoundLayout', {main: 'App_notFound'});
    },
};

//Login router
FlowRouter.route('/login',{
    name: 'App.login',
    action(){
        BlazeLayout.render('loginLayout',{main: 'loginMain'})
    }
});

//Admin routers
FlowRouter.route('/admin',{
    name: 'App.admin',
    action(){
        if(Meteor.user()){
            Session.set('actualPage','Escritorio');
            BlazeLayout.render('adminLayout',{main: 'adminMain'});
        }else{
            FlowRouter.go('/');
        }
    }
});
FlowRouter.route('/usuarios',{
    name: 'App.users',
    action(){
        if(Meteor.user()){
            Session.set('actualPage','Gestion de usuarios');
            BlazeLayout.render('adminLayout',{main: 'usersMain'})
        }else{
            FlowRouter.go('/');
        }
    }
});
FlowRouter.route('/categorias',{
    name: 'App.categories',
    action(){
        if(Meteor.user()){
            Session.set('actualPage','Gestion de categorias');
            BlazeLayout.render('adminLayout',{main: 'categoriesMain'})
        }else{
            FlowRouter.go('/');
        }
    }
});
FlowRouter.route('/sub-categorias',{
    name: 'App.subCategories',
    action(){
        if(Meteor.user()){
            Session.set('actualPage','Gestion de sub-categorias');
            BlazeLayout.render('adminLayout',{main: 'subCategoriesMain'})
        }else{
            FlowRouter.go('/');
        }
    }
});
FlowRouter.route('/lineas',{
    name: 'App.Lines',
    action(){
        if(Meteor.user()){
            Session.set('actualPage','Gestion de lineas');
            BlazeLayout.render('adminLayout',{main: 'linesMain'})
        }else{
            FlowRouter.go('/');
        }
    }
});
FlowRouter.route('/proveedores',{
    name: 'App.Lines',
    action(){
        if(Meteor.user()){
            Session.set('actualPage','Gestion de proveedores');
            BlazeLayout.render('adminLayout',{main: 'suppliersMain'})
        }else{
            FlowRouter.go('/');
        }
    }
});
FlowRouter.route('/productos',{
    name: 'App.Products',
    action(){
        if(Meteor.user()){
            Session.set('actualPage','Gestion de productos');
            BlazeLayout.render('adminLayout',{main: 'productsMain'})
        }else{
            FlowRouter.go('/');
        }
    }
});
FlowRouter.route('/configuracion',{
    name: 'App.Products',
    action(){
        if(Meteor.user()){
            Session.set('actualPage','Configuración');
            BlazeLayout.render('adminLayout',{main: 'configurationMain'})
        }else{
            FlowRouter.go('/');
        }
    }
});
FlowRouter.route('/firmas',{
    name: 'App.Products',
    action(){
        if(Meteor.user()){
            Session.set('actualPage','Gestion de firmas');
            BlazeLayout.render('adminLayout',{main: 'fingersMain'})
        }else{
            FlowRouter.go('/');
        }
    }
});
FlowRouter.route('/div-sub-categorias',{
    name: 'App.Products',
    action(){
        if(Meteor.user()){
            Session.set('actualPage','Gestion de división SubCategorias');
            BlazeLayout.render('adminLayout',{main: 'divSubCategoriesMain'})
        }else{
            FlowRouter.go('/');
        }
    }
});
