/**
 * Created by alejandrolemusrodriguez on 23/01/17.
 */
export class Constans{
    constructor(){
    }
    static get CATEGORIES_IMG(){
        return 'public/images/categories_image/';
    }
    static get PRODUCTS_IMG(){
        return 'public/images/products_image/';
    }
    static get FINGERS_IMG(){
        return 'public/images/fingers/';
    }
    static get PAGE_IMG_PRINCIPAL(){
        return 'public/images/configuration/test.jpg';
    }
    static get PAGE_IMG_SECONDARY(){
        return 'public/images/configuration/silla.jpg';
    }
}