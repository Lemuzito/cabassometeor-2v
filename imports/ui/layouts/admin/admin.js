/**
 * Created by alejandrolemusrodriguez on 17/01/17.
 */
import './admin.html';

Template.adminLayout.helpers({
    siteName : () =>{
        return Session.get('actualPage');
    },
    currentUser : () =>{
        return Meteor.user();
    }
});
Template.adminLayout.events({
   'click .exit-app': (event) => {
       event.preventDefault();
       Meteor.logout(function(error){
           if(!error){
               FlowRouter.go('/login');
           }
       });
   }
});