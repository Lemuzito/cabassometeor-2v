/**
 * Created by alejandrolemusrodriguez on 17/01/17.
 */
import './login.html';

Template.loginLayout.onRendered(function() {
    $('body').addClass('back-image');
});

Template.loginLayout.onDestroyed(function() {
    // remove the class to it does not appear on other routes
    $('body').removeClass('back-image');
});