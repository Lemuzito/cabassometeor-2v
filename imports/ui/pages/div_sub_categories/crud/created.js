/**
 * Created by alejandrolemusrodriguez on 25/01/17.
 */
import './created.html';
import {SubCategories} from '../../../../api/sub_categories/sub_categories.js';
import { insert } from '../../../../api/div_sub_categories/methods.js';
Template.divSubCategoryCreated.onCreated(function () {
   Meteor.subscribe('subCategories');
});
Template.divSubCategoryCreated.helpers({
    listSubCategories:()=>{
        return SubCategories.find();
    }
});
Template.divSubCategoryCreated.events({
   'submit #div-sub-category-created-form': function(event) {
        event.preventDefault();
        const form = event.target;
        const name = form.name.value;
        const subCategory = form.subCategory.value;
        if(name&&subCategory) {
            const data = {name: name, subCategory: subCategory};
            const div = insert.call(data, (error) => {
                if (error) {
                    console.log(error);
                    swal("Error", "Hubo un error al guardar", "error");
                }
                else {
                    $('#div-sub-categories-modal-create').modal('hide');
                    event.target.name.value = '';
                    swal({
                        title: "¡División de sub-categoria agregada!",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "success"
                    });
                }
            });
        }
        else {
            swal("Error", "Hubo un error, campos incompletos", "error");
        }
   }
});