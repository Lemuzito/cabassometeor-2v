/**
 * Created by alejandrolemusrodriguez on 25/01/17.
 */
import './main.html';
import '../../components/modals/simple_modal.js';
import '../../components/button_add/button_add.js';
import './crud/created.js';
import {DivSubCategories} from '../../../api/div_sub_categories/div_sub_categories.js';
import {SubCategories} from  '../../../api/sub_categories/sub_categories.js';
import {DeletedDivSubCategory} from './crud/deleted.js';

Template.divSubCategoriesMain.onCreated(function () {
   Meteor.subscribe('divSubCategories');
   Meteor.subscribe('subCategories');
});
Template.divSubCategoriesMain.helpers({
   listDivSubCategories:()=>{
       return DivSubCategories.find();
   },
    getNameSubcategory:(id)=>{
       try{
           return SubCategories.findOne({_id: id}).name;
       }catch (e){

       }
    }
});
Template.divSubCategoriesMain.events({
   'click .remove-div-sub-category':function (event) {
       event.preventDefault();
       console.log("f")
       const id = this._id;
       const remove = new DeletedDivSubCategory(id);
   }
});