/**
 * Created by alejandrolemusrodriguez on 24/01/17.
 */
import './main.html';
import '../../components/modals/simple_modal.js';
import '../../components/button_add/button_add.js';
import './crud/created.js';
import {Lines} from '../../../api/lines/lines';
import {Suppliers} from '../../../api/suppliers/suppliers';
import {DeletedLines} from './crud/deleted.js';

Template.linesMain.onCreated(function () {
    Meteor.subscribe('lines');
    Meteor.subscribe('suppliers');
});
Template.linesMain.helpers({
    listLines: () => {
        return Lines.find();
    },
    getSupplier: (id) => {
       try {
           return Suppliers.findOne({_id: id}).name;
       }catch (e){

       }
    }
});
Template.linesMain.events({
   'click .remove-line':function (event) {
       event.preventDefault();
       const id = this._id;
       const remove = new DeletedLines(id);
   }
});