/**
 * Created by lemux-dev on 27/01/17.
 */
import {remove} from '../../../../api/lines/methods.js';
export class DeletedLines{
    constructor(id){
        this.id = id;
        const data = {_id: this.id};
        const remove_lines= remove.call(data, (err)=>{
            if(err){
                swal("Error", "Hubo un error al borra", "error");
            }
            else {
                swal({
                    title: "¡Linea eliminada!",
                    timer: 2000,
                    showConfirmButton: false,
                    type: "success"
                });
            }
        });
    }
}