/**
 * Created by alejandrolemusrodriguez on 24/01/17.
 */
import './created.html';
import {Suppliers} from '../../../../api/suppliers/suppliers';
import {insert} from '../../../../api/lines/methods.js';

Template.linesCreated.onCreated(function () {
   Meteor.subscribe('suppliers');
});
Template.linesCreated.helpers({
   listSuppliers:()=>{
       return Suppliers.find();
   }
});
Template.linesCreated.events({
    'submit #lines-created-form':function(event){
        event.preventDefault();
        const form = event.target;
        const name = form.name.value;
        const supplier = form.supplier.value;
        if(name && supplier){
            const data = {name: name, supplier: supplier};
            const line = insert.call(data, (error)=>{
                if (error) {
                    console.log(error);
                    swal("Error", "Hubo un error al guardar", "error");
                }
                else {
                    $('#line-modal-create').modal('hide');
                    event.target.name.value = '';
                    swal({
                        title: "¡Linea agregada!",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "success"
                    });
                }
            });
        }else{
            swal("Error", "Hubo un error, campos incompletos", "error");
        }
    }
});

