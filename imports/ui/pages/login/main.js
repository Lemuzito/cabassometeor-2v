/**
 * Created by alejandrolemusrodriguez on 17/01/17.
 */
import './main.html';
import { FlowRouter } from 'meteor/kadira:flow-router';

Template.loginMain.events({
    'submit #login-form': function (event, instance) {
        event.preventDefault();
        const form = event.target;
        const username = form.username.value;
        const pass = form.password.value;
        if (username && pass) {
            Meteor.loginWithPassword(username, pass, function(error){
                if(error){
                    swal("¡UPS!", "Credenciales incorrectas", "error")
                }else{
                    if(Meteor.user()){
                        FlowRouter.go('/admin');
                    }
                }
            });
        }
        else {
            swal("¡UPS!", "Credenciales incorrectas", "error");
        }
    }
});