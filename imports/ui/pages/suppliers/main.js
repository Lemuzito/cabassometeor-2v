/**
 * Created by alejandrolemusrodriguez on 24/01/17.
 */
import './main.html';
import '../../components/modals/simple_modal.js';
import '../../components/button_add/button_add.js';
import './crud/created.js';
import {Suppliers} from '../../../api/suppliers/suppliers.js';
import {DeletedSuppliers} from './crud/deleted.js';


Template.suppliersMain.onCreated(function () {
    Meteor.subscribe('suppliers');
});
Template.suppliersMain.helpers({
   listSuppliers:()=>{
       return Suppliers.find();
   }
});
Template.suppliersMain.events({
    'click .remove-suppliers':function (event) {
        event.preventDefault();
        const id = this._id;
        const remove = new DeletedSuppliers(id);
    }
});