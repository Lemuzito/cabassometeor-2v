/**
 * Created by alejandrolemusrodriguez on 24/01/17.
 */
import './created.html'
import {insert} from '../../../../api/suppliers/methods.js';
Template.suppliersCreated.events({
   'submit #suppliers-created-form':function(event){
       event.preventDefault();
       const form = event.target;
       const name = form.name.value;
       if(name){
           const data = {name: name};
           const supplier = insert.call(data, (error)=>{
               if (error) {
                   console.log(error);
                   swal("Error", "Hubo un error al guardar", "error");
               }
               else {
                   $('#suppliers-modal-create').modal('hide');
                   event.target.name.value = '';
                   swal({
                       title: "¡Proveedor agregado!",
                       timer: 2000,
                       showConfirmButton: false,
                       type: "success"
                   });
               }
           });
       }else{
           swal("Error", "Hubo un error, campos incompletos", "error");
       }
   }
});