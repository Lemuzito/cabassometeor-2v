/**
 * Created by lemux-dev on 27/01/17.
 */
import {remove} from '../../../../api/suppliers/methods.js';
export class DeletedSuppliers{
    constructor(id){
        this.id = id;
        const data = {_id: this.id};
        const remove_suppliers= remove.call(data, (err)=>{
            if(err){
                swal("Error", "Hubo un error al borra", "error");
            }
            else {
                swal({
                    title: "¡Proveedor eliminado!",
                    timer: 2000,
                    showConfirmButton: false,
                    type: "success"
                });
            }
        });
    }
}