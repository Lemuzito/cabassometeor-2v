/**
 * Created by lemux-dev on 27/01/17.
 */
import {remove} from '../../../../api/sub_categories/methods.js';
export class DeletedSubCategory{
    constructor(id){
        this.id = id;
        const data = {_id: this.id};
        const remove_sub_category= remove.call(data, (err)=>{
            if(err){
                swal("Error", "Hubo un error al borra", "error");
            }
            else {
                swal({
                    title: "¡SubCategoria eliminada!",
                    timer: 2000,
                    showConfirmButton: false,
                    type: "success"
                });
            }
        });
    }
}