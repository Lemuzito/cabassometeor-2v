/**
 * Created by alejandrolemusrodriguez on 23/01/17.
 */
import './created.html';
import {Categories}from '../../../../api/categories/categories.js';
import {insert} from '../../../../api/sub_categories/methods.js';
Template.subCategoryCreated.onCreated(function () {
    Meteor.subscribe('categories');
});
Template.subCategoryCreated.helpers({
    listCategories:()=>{
        return Categories.find();
    }
});
Template.subCategoryCreated.events({
    'submit #sub-category-created-form': function(event){
        event.preventDefault();
        const form = event.target;
        const name = form.name.value;
        const category = form.category.value;
        if(name&&category){
            const data = {name: name, category: category};
            const subCategory = insert.call(data, (error) => {
                if (error) {
                    console.log(error);
                    swal("Error", "Hubo un error al guardar", "error");
                }
                else {
                    event.target.name.value='';
                    $('#sub-category-modal-create').modal('hide');
                    swal({
                        title: "¡Categoria agregada!",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "success"
                    });
                }
            });
        }else {
            swal("Error", "Hubo un error, campos incompletos", "error");
        }
    }
});