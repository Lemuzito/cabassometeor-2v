/**
 * Created by alejandrolemusrodriguez on 23/01/17.
 */
import './main.html';
import '../../components/modals/simple_modal.js';
import '../../components/button_add/button_add.js';
import './crud/created.js';
import {SubCategories} from '../../../api/sub_categories/sub_categories';
import {Categories} from '../../../api/categories/categories';
import {DeletedSubCategory} from './crud/deleted.js';

Template.subCategoriesMain.onCreated(function () {
    Meteor.subscribe('subCategories');
    Meteor.subscribe('categories');
});
Template.subCategoriesMain.helpers({
    listSubCategories: () => {
        return SubCategories.find();
    },
    getCategory: (id) => {
        try{
            return Categories.findOne({_id: id}).name;
        }catch (e){

        }
    }
});
Template.subCategoriesMain.events({
    'click .remove-sub-category':function (event) {
        event.preventDefault();
        const id = this._id;
        const remove = new DeletedSubCategory(id);
    }
});