/**
 * Created by alejandrolemusrodriguez on 25/01/17.
 */
import './created.html';
import {Fingers} from '../../../../api/fingers/fingers.js';
import {insert} from '../../../../api/fingers/methods.js';
import {Configuration} from '../../../../api/configuration/configuration.js';

Template.fingerCreated.onCreated(function () {
    Meteor.subscribe('fingers');
    Meteor.subscribe('configuration');
});

Template.fingerCreated.helpers({
    imageExist: () => {
        return Session.get('imageExist');
    },
    image: () => {
        return Session.get('image');
    },
    urlImage: () => {
        return Session.get('urlImage');
    },
    noImage:()=>{
        return Configuration.findOne().urlStaticServer+'public/images/configuration/no_image.png';
    }
});
Template.fingerCreated.events({
    'submit #finger-created-form':function (event) {
        event.preventDefault();
        const form = event.target;
        const name = form.name.value;
        const image = Session.get('imageSave');
        if(name&&image){
            const data = {name:name};
            const finger = insert.call(data, (error)=>{
                if (error) {
                    console.log(error);
                    swal("Error", "Hubo un error al guardar", "error");
                }
                else {
                    const finger = Fingers.find().fetch().reverse();
                    const id = finger[0]._id;
                    const dataImage = {token:'V2w165c8N6P5U484QQNJn3U1vWMVad1H', image: image,id:id}
                    Meteor.call('uploadImageFinger',dataImage,function(err){
                        console.log(err)
                    });
                    $('#finger-modal-create').modal('hide');
                    event.target.name.value = '';
                    swal({
                        title: "¡Firma agregada!",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "success"
                    });
                }
            });
        }else{
            swal("Error", "Hubo un error, campos incompletos", "error");
        }
    },
    'change .image-finger': (event) => {
        event.preventDefault();
        var fr = new FileReader();
        var image;
        fr.onload = function (e) {
            image = e.target.result.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
            Session.set('imageSave', image[2]);
            Session.set('image', e.target.result);
            Session.set('imageExist', true);
        };
        fr.readAsDataURL(event.target.files[0]);
        Session.set('urlImage', event.target.value.replace(/.*[\/\\]/, ''));
    },
});