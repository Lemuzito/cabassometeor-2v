/**
 * Created by lemux-dev on 27/01/17.
 */
import {remove} from '../../../../api/fingers/methods.js';
export class DeletedFinger{
    constructor(id){
        this.id = id;
        const data = {_id: this.id};
        const remove_finger= remove.call(data, (err)=>{
            if(err){
                swal("Error", "Hubo un error al borra", "error");
            }
            else {
                swal({
                    title: "¡Firma eliminada!",
                    timer: 2000,
                    showConfirmButton: false,
                    type: "success"
                });
            }
        });
    }
}