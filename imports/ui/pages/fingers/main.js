/**
 * Created by alejandrolemusrodriguez on 25/01/17.
 */
import './main.html';
import '../../components/modals/simple_modal.js';
import '../../components/button_add/button_add.js';
import './crud/created.js';
import {Fingers} from '../../../api/fingers/fingers.js';
import {Constans} from '../../utils/constans.js';
import {DeletedFinger} from './crud/deleted.js';
import {Configuration} from '../../../api/configuration/configuration.js';
Template.fingersMain.onCreated(function () {
 Meteor.subscribe('configuration');
 Meteor.subscribe('fingers');
});
Template.fingersMain.helpers({
   listFingers:()=>{
       return Fingers.find();
   },
    urlImage:()=>{
       if(Configuration.findOne()){
           return Configuration.findOne().urlStaticServer+Constans.FINGERS_IMG;
       }

    },
    getLink:(id)=>{
        if(Configuration.findOne()) {
            return Configuration.findOne().urlStaticServer + Constans.FINGERS_IMG + id + '.jpg';
        }
    }
});

Template.fingersMain.events({
   'click .remove-finger':function (event) {
       event.preventDefault();
       const id = this._id;
       const remove = new DeletedFinger(id);
   }
});