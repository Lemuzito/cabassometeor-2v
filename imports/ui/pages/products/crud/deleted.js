/**
 * Created by lemux-dev on 27/01/17.
 */
import {remove} from '../../../../api/products/methods.js';
export class DeletedProducts{
    constructor(id){
        this.id = id;
        const data = {_id: this.id};
        const remove_product= remove.call(data, (err)=>{
            if(err){
                swal("Error", "Hubo un error al borra", "error");
            }
            else {
                swal({
                    title: "¡Producto eliminado!",
                    timer: 2000,
                    showConfirmButton: false,
                    type: "success"
                });
            }
        });
    }
}