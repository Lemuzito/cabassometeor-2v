/**
 * Created by alejandrolemusrodriguez on 24/01/17.
 */
import './created.html';
import {SubCategories} from '../../../../api/sub_categories/sub_categories.js';
import {Lines} from '../../../../api/lines/lines.js';
import {insert} from '../../../../api/products/methods.js';
import {Products} from '../../../../api/products/products.js';
import {DivSubCategories} from '../../../../api/div_sub_categories/div_sub_categories.js';
import {Configuration} from '../../../../api/configuration/configuration.js';
Template.productCreated.onCreated(function () {
    Meteor.subscribe('subCategories');
    Meteor.subscribe('products');
    Meteor.subscribe('lines');
    Meteor.subscribe('divSubCategories');
    Meteor.subscribe('configuration');
    var instance = this;
    instance.searchSubcatgory = new ReactiveVar({name: {$regex: ''}});
    instance.subCategories = new ReactiveVar([]);
    Session.set('showList', true);
});

Template.productCreated.helpers({
    listSubCategories: () => {
        return SubCategories.find(Template.instance().searchSubcatgory.get());
    },
    showList: () => {
        return Session.get('showList');
    },
    showSubcategories: ()=>{
        return Template.instance().subCategories.get();
    },
    getNameSubCategory:(id)=>{
        try{
            return SubCategories.findOne({_id: id}).name;
        }catch (e){

        }
    },
    imageExist: () => {
        return Session.get('imageExist');
    },
    image: () => {
        return Session.get('image');
    },
    urlImage: () => {
        return Session.get('urlImage');
    },
    listLines:()=>{
        return Lines.find();
    },
    divSubCategory:(id)=>{
        const num = DivSubCategories.find({subCategory:id}).count();
        if(num == 0){
            return false;
        }else {
            return true;
        }
    },
    showListDivSubCategories:(id)=>{
        return DivSubCategories.find({subCategory:id});
    },
    noImage:()=>{
        return Configuration.findOne().urlStaticServer+'public/images/configuration/no_image.png';
    }
});

Template.productCreated.events({
    'keyup .search-sub-category': function (event, instance) {
        event.preventDefault();
        const key = event.target.value;
        if (key.length == 0) {
            Session.set('showList', true);
        } else {
            Session.set('showList', false);
        }
        instance.searchSubcatgory.set({name: {$regex: key}});
    },
    'click .select-sub-category': function (event, instance) {
        event.preventDefault();
        const id = event.target.id;
        var subCategories = instance.subCategories.get();
        var success = true;
        subCategories.forEach(function (subCategory) {
            if(subCategory.id == id){
                swal("Error", "SubCategoria ya seleccionada", "error");
                success = false;
            }
        });
        if(success){
            subCategories.push({id:id,idDivSubCategory:'Does not exist'});
            instance.subCategories.set(subCategories);
        }
    },
    'change .div-subcategory':function (event, instance) {
        event.preventDefault();
        const idSubcategory = event.target.id;
        var subCategories = instance.subCategories.get();
        const idDivSubCategory = event.target.value;
        subCategories.forEach(function (subCategory) {
            if(subCategory.id == idSubcategory){
                subCategory.idDivSubCategory = idDivSubCategory;
            }
        });
        instance.subCategories.set(subCategories);
    }
    ,
    'click .cancel-category': function (event, instance) {
        event.preventDefault();
        const id = event.target.id;
        var index = 0;
        var indexOff = 0;
        var subCategories = instance.subCategories.get();
        subCategories.forEach(function (subCategory) {
            if(subCategory.id == id){
                indexOff = index;
            }else {
                index++;
            }
        });
        subCategories.splice(indexOff, 1);
        instance.subCategories.set(subCategories);
    },
    'change .image-product': (event) => {
        event.preventDefault();
        console.log(event.target);
        var fr = new FileReader();
        var image;
        fr.onload = function (e) {
            image = e.target.result.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
            Session.set('imageSave', image[2]);
            Session.set('image', e.target.result);
            Session.set('imageExist', true);
        };
        fr.readAsDataURL(event.target.files[0]);
        Session.set('urlImage', event.target.value.replace(/.*[\/\\]/, ''));
    },
    'submit #product-created-form': function (event, instance) {
        event.preventDefault();
        const form = event.target;
        const name = form.name.value;
        const line = form.line.value;
        const image = Session.get('imageSave');
        const subcategories = instance.subCategories.get();

        if(name && line && image && subcategories.length != 0) {
            const data = {name: name, line: line, subcategories: subcategories};
            const product = insert.call(data, (error) => {
                if (error) {
                    console.log(error);
                    swal("Error", "Hubo un error al guardar", "error");
                }
                else {
                    event.target.name.value='';
                    event.target.subcategories.value='';
                    const product = Products.find().fetch().reverse();
                    const id = product[0]._id;
                    const data = {token:'V2w165c8N6P5U484QQNJn3U1vWMVad1H',image: image,id:id};
                    Meteor.call('uploadImageProduct',data,(error)=> {
                        console.log(error)
                    });
                    $('#product-modal-create').modal('hide');
                    swal({
                        title: "¡Producto agregado!",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "success"
                    });
                }
            });
        }
        else{
            swal("Error", "Error, Campos incompletos", "error");
        }
    }
});