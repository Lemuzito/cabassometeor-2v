/**
 * Created by alejandrolemusrodriguez on 24/01/17.
 */
import './main.html';
import '../../components/modals/large_modal.js';
import '../../components/modals/simple_modal.js';
import '../../components/button_add/button_add.js';
import './crud/created.js';
import {Products} from '../../../api/products/products.js';
import {SubCategories} from '../../../api/sub_categories/sub_categories.js';
import {Lines} from '../../../api/lines/lines.js';
import {DivSubCategories} from '../../../api/div_sub_categories/div_sub_categories.js';
import {Constans} from '../../utils/constans.js';
import {DeletedProducts} from './crud/deleted.js';
import {Configuration} from '../../../api/configuration/configuration.js';
Template.productsMain.onCreated(function () {
    Meteor.subscribe('products');
    Meteor.subscribe('subCategories');
    Meteor.subscribe('lines');
    Meteor.subscribe('divSubCategories');
    Meteor.subscribe('configuration');
    Session.set('subCategories', '')
});
Template.productsMain.helpers({
    listProducts: () => {
        return Products.find();
    },
    urlImage: () => {
        if (Configuration.findOne()) {
            return Configuration.findOne().urlStaticServer + Constans.PRODUCTS_IMG;
        }
    },
    subCategories: () => {
        try {
            if (Session.get('subCategories') != '') {
                return Session.get('subCategories');
            }
        } catch (e) {

        }
    },
    getNameSubcategory: (id) => {
        try {
            return SubCategories.findOne({_id: id}).name;
        } catch (e) {

        }
    },
    getNameLine: (id) => {
        try {
            return Lines.findOne({_id: id}).name;
        } catch (e) {

        }
    },
    getNameDivSubcategory: (id) => {
        try {
            return DivSubCategories.findOne({_id: id}).name;
        }
        catch (e) {
            return "No tiene división"
        }
    }
});
Template.productsMain.events({
    'click .view-sub-categories': function (event) {
        event.preventDefault();
        Session.set('subCategories', this.subcategories);
    },
    'click .remove-product': function (event) {
        event.preventDefault();
        const id = this._id;
        const remove = new DeletedProducts(id);
    }
});