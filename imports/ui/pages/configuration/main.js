/**
 * Created by alejandrolemusrodriguez on 25/01/17.
 */
import './main.html';
import {Configuration} from '../../../api/configuration/configuration.js';
import {updatePrincipalPage} from '../../../api/configuration/methods.js';
import {updateUrlStaticServer} from '../../../api/configuration/methods.js';
import {updateServicesConfiguration} from '../../../api/configuration/methods.js';
import {Constans} from '../../utils/constans.js';
Template.configurationMain.onCreated(function () {
    const instance = this;
    instance.icons = new ReactiveVar([]);
    Meteor.subscribe('configuration');
});
Template.configurationMain.helpers({
    icon1: () => {
        return Template.instance().icons.get()[0];
    },
    icon2: () => {
        return Template.instance().icons.get()[1];
    },
    icon3: () => {
        return Template.instance().icons.get()[2];
    },
    icon4: () => {
        return Template.instance().icons.get()[3];
    },
    principalConfiguration: () => {
        return Configuration.findOne();
    },
    imageExist1: () => {
        return Session.get('imageExist1');
    },
    image1: () => {
        return Session.get('image1');
    },
    urlImage1: () => {
        return Session.get('urlImage1');
    },
    imageExist2: () => {
        return Session.get('imageExist2');
    },
    image2: () => {
        return Session.get('image2');
    },
    urlImage2: () => {
        return Session.get('urlImage2');
    },
    imagePrincipal: () => {
        if (Configuration.findOne()) {
            return Configuration.findOne().urlStaticServer + Constans.PAGE_IMG_PRINCIPAL;
        }
    },
    imageSecondary: () => {
        if (Configuration.findOne()) {
            return Configuration.findOne().urlStaticServer + Constans.PAGE_IMG_SECONDARY;
        }
    }
});
Template.configurationMain.events({
    'change .icon-1': function (event, instance) {
        event.preventDefault();
        var icons = instance.icons.get();
        icons[0] = event.target.value;
        instance.icons.set(icons);
    },
    'change .icon-2': function (event, instance) {
        event.preventDefault();
        var icons = instance.icons.get();
        icons[1] = event.target.value;
        instance.icons.set(icons);
    },
    'change .icon-3': function (event, instance) {
        event.preventDefault();
        var icons = instance.icons.get();
        icons[2] = event.target.value;
        instance.icons.set(icons);
    },
    'change .icon-4': function (event, instance) {
        event.preventDefault();
        var icons = instance.icons.get();
        icons[3] = event.target.value;
        instance.icons.set(icons);
    }
});
Template.configurationMain.events({
    'submit #principal-page': function (event) {
        event.preventDefault();
        const form = event.target;
        const name = form.name.value;
        const facebook = form.facebook.value;
        const phone = form.phone.value;
        const email = form.email.value;
        const primarySlogan = form.primarySlogan.value;
        const secondarySlogan = form.secondarySlogan.value;
        if (name && facebook && phone && email && primarySlogan && secondarySlogan) {
            const data = {
                _id: Configuration.findOne({})._id,
                name: name,
                facebook: facebook,
                phone: phone,
                email: email,
                primarySlogan: primarySlogan,
                secondarySlogan: secondarySlogan
            };
            const u = updatePrincipalPage.call(data, (error) => {
                swal({
                    title: "¡Configuración actualizada!",
                    timer: 2000,
                    showConfirmButton: false,
                    type: "success"
                });
            });
        } else {
            swal("Error", "Hubo un error, campos incompletos", "error");
        }
    },
    'change .image-principal': function (event, instance) {
        event.preventDefault();
        var fr = new FileReader();
        var image;
        fr.onload = function (e) {
            image = e.target.result.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
            Session.set('imageSave1', image[2]);
            Session.set('image1', e.target.result);
            Session.set('imageExist1', true);
        };
        fr.readAsDataURL(event.target.files[0]);
        Session.set('urlImage1', event.target.value.replace(/.*[\/\\]/, ''));
    },
    'change .image-secondary': function (event, instance) {
        event.preventDefault();
        var fr = new FileReader();
        var image;
        fr.onload = function (e) {
            image = e.target.result.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
            Session.set('imageSave2', image[2]);
            Session.set('image2', e.target.result);
            Session.set('imageExist2', true);
        };
        fr.readAsDataURL(event.target.files[0]);
        Session.set('urlImage2', event.target.value.replace(/.*[\/\\]/, ''));
    },
    'submit #save-image-and-url': function (event) {
        event.preventDefault();
        const urlStaticServer = event.target.urlStaticServer.value;
        const img1 = Session.get('imageSave1');
        const img2 = Session.get('imageSave2');
        if (urlStaticServer) {
            const data = {urlStaticServer: urlStaticServer, _id: Configuration.findOne({})._id};
            if (img1) {
                if (img2) {
                    Meteor.call('uploadImagePrincipal', {image: img1}, (err) => {
                        if (!err) {
                            Session.set('imageSave1', undefined);
                            Session.set('image1', undefined);
                            Session.set('imageExist1', false);
                            Session.set('urlImage1', undefined);
                        }
                    });
                    Meteor.call('uploadImageSecondary', {image: img2}, (err) => {
                        if (!err) {
                            Session.set('imageSave2', undefined);
                            Session.set('image2', undefined);
                            Session.set('imageExist2', false);
                            Session.set('urlImage2', undefined);
                        }
                    });
                    const url = updateUrlStaticServer.call(data, (error) => {
                        if (!error) {
                            swal({
                                title: "¡Url del servidor estatico actualizada!",
                                timer: 2000,
                                showConfirmButton: false,
                                type: "success"
                            });
                        } else {
                            swal("Error", "Hubo un error, Al momento de guardar", "error");
                        }

                    });

                } else {
                    Meteor.call('uploadImagePrincipal', {image: img1}, (err) => {
                        if (!err) {
                            Session.set('imageSave1', undefined);
                            Session.set('image1', undefined);
                            Session.set('imageExist1', false);
                            Session.set('urlImage1', undefined);
                        }
                    });
                    const url = updateUrlStaticServer.call(data, (error) => {
                        if (!error) {
                            swal({
                                title: "¡Url del servidor estatico actualizada!",
                                timer: 2000,
                                showConfirmButton: false,
                                type: "success"
                            });
                        } else {
                            swal("Error", "Hubo un error, Al momento de guardar", "error");
                        }

                    });
                }
            } else {
                if (img2) {
                    Meteor.call('uploadImageSecondary', {image: img2}, (err) => {
                        if (!err) {
                            Session.set('imageSave2', undefined);
                            Session.set('image2', undefined);
                            Session.set('imageExist2', false);
                            Session.set('urlImage2', undefined);
                        }
                    });
                    const url = updateUrlStaticServer.call(data, (error) => {
                        if (!error) {
                            swal({
                                title: "¡Url del servidor estatico actualizada!",
                                timer: 2000,
                                showConfirmButton: false,
                                type: "success"
                            });
                        } else {
                            swal("Error", "Hubo un error, Al momento de guardar", "error");
                        }

                    });
                } else {
                    const url = updateUrlStaticServer.call(data, (error) => {
                        if (!error) {
                            swal({
                                title: "¡Url del servidor estatico actualizada!",
                                timer: 2000,
                                showConfirmButton: false,
                                type: "success"
                            });
                        } else {
                            swal("Error", "Hubo un error, Al momento de guardar", "error");
                        }

                    });
                }
            }
        } else {
            swal("Error", "Hubo un error, No se encuentra la URL del servidor estatico", "error");
        }
    },
    'submit #saveServicesConfiguration': function (event) {
        event.preventDefault();
        const form = event.target;
        var count = 0;
        const services = [
            {title: form.title1.value, description: form.description1.value, icon: form.icon1.value},
            {title: form.title2.value, description: form.description2.value, icon: form.icon2.value},
            {title: form.title3.value, description: form.description3.value, icon: form.icon3.value},
            {title: form.title4.value, description: form.description4.value, icon: form.icon4.value}
        ];
        services.forEach(function (service) {
            if (service.icon && service.title && service.description) {

            } else {
                count++;
            }
        });
        if (count == 0) {
            const data = {
                _id: Configuration.findOne()._id,
                servicesTitle1:form.title1.value,
                servicesTitle2:form.title2.value,
                servicesTitle3:form.title3.value,
                servicesTitle4:form.title4.value,
                servicesDescription1:form.description1.value,
                servicesDescription2:form.description2.value,
                servicesDescription3:form.description3.value,
                servicesDescription4:form.description4.value,
                servicesIcon1:form.icon1.value,
                servicesIcon2:form.icon2.value,
                servicesIcon3:form.icon3.value,
                servicesIcon4:form.icon4.value,
            };
            const updateServices = updateServicesConfiguration.call(data, (error) => {
                if (!error) {
                    swal({
                        title: "¡Los servicios han sido actualizados!",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "success"
                    });
                }
            });
        } else {
            swal("Error", "Hubo un error, existe un campo vacio", "error");
        }
    }
});