/**
 * Created by alejandrolemusrodriguez on 20/01/17.
 */
import './main.html';
import '../../components/modals/simple_modal.js';
import '../../components/button_add/button_add.js';
import './crud/created.js';
import {Categories} from '../../../api/categories/categories.js';
import {DeletedCategory} from './crud/deleted.js';
import {Constans} from '../../utils/constans.js';
import {Configuration} from '../../../api/configuration/configuration.js';

Template.categoriesMain.onCreated(function () {
    Meteor.subscribe('categories');
    Meteor.subscribe('configuration');
});

Template.categoriesMain.helpers({
   listCategories:()=>{
        return Categories.find();
   },
   urlImage:()=>{
       if(Configuration.findOne()){
           return Configuration.findOne().urlStaticServer+Constans.CATEGORIES_IMG;
       }
   }
});
Template.categoriesMain.events({
    'click .remove-category':function(event){
        event.preventDefault();
        const id = this._id;
        const remove = new DeletedCategory(id);
    }
})