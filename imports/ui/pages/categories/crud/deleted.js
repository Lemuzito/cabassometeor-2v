/**
 * Created by lemux-dev on 27/01/17.
 */
import {remove} from '../../../../api/categories/methods.js';
export class DeletedCategory{
    constructor(id){
        this.id = id;
        const data = {_id: this.id};
        const remove_category = remove.call(data, (err)=>{
            if(err){
                swal("Error", "Hubo un error al borra", "error");
            }
            else {
                swal({
                    title: "¡Categoria eliminado!",
                    timer: 2000,
                    showConfirmButton: false,
                    type: "success"
                });
            }
        });
    }
}