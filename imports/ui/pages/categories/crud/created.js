/**
 * Created by alejandrolemusrodriguez on 20/01/17.
 */
import './created.html';
import {insert} from '../../../../api/categories/methods.js';
import {Categories} from '../../../../api/categories/categories.js';
import {Configuration} from '../../../../api/configuration/configuration.js';
Template.categoryCreated.onCreated(function () {
    Meteor.subscribe('categories');
    Meteor.subscribe('configuration');
});
Template.categoryCreated.events({
    'change .image-category': (event) => {
        event.preventDefault();
        var fr = new FileReader();
        var image;
        fr.onload = function (e) {
            image = e.target.result.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
            Session.set('imageSave', image[2]);
            Session.set('image', e.target.result);
            Session.set('imageExist', true);
        };
        fr.readAsDataURL(event.target.files[0]);
        Session.set('urlImage', event.target.value.replace(/.*[\/\\]/, ''));
    },
    'submit #category-created-form': (event) => {
        event.preventDefault();
        const form = event.target;
        const name = form.name.value;
        const img = Session.get('imageSave');

        if (name && img) {
            const data = {name: name};
            const category = insert.call(data, (error) => {
                if (error) {
                    console.log(error);
                    swal("Error", "Hubo un error al guardar", "error");
                }
                else {
                    const category = Categories.find().fetch().reverse();
                    const id = category[0]._id;
                    const dataImage = {token:'V2w165c8N6P5U484QQNJn3U1vWMVad1H', image: img,id:id}
                    Meteor.call('uploadImageCategory',dataImage,function(err){

                    });
                    $('#category-modal-create').modal('hide');
                    swal({
                        title: "¡Categoria agregada!",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "success"
                    });
                }
            });
        } else {
            swal("Error", "Hubo un error, campos incompletos", "error");
        }
    }
});

Template.categoryCreated.helpers({
    imageExist: () => {
        return Session.get('imageExist');
    },
    image: () => {
        return Session.get('image');
    },
    urlImage: () => {
        return Session.get('urlImage');
    },
    noImage:()=>{
        return Configuration.findOne().urlStaticServer+'public/images/configuration/no_image.png';
    }
});

