/**
 * Created by alejandrolemusrodriguez on 20/01/17.
 */
import './main.html';
import '../../components/modals/simple_modal.js';
import '../../components/button_add/button_add.js';
import './crud/created.js';
import {DeletedUsers} from './crud/deleted.js';
import {Configuration} from '../../../api/configuration/configuration.js';
Template.usersMain.onCreated(function () {
   Meteor.subscribe('users');
   Meteor.subscribe('configuration');
});

Template.usersMain.helpers({
   listUsers:()=>{
       return Meteor.users.find();
   },
    noImage:()=>{
       return Configuration.findOne().urlStaticServer+'public/images/configuration/no_image.png';
    }
});
Template.usersMain.events({
    'click .remove-user':function (event) {
        event.preventDefault();
        const id = this._id;
        const remove = new DeletedUsers(id);
    }
});