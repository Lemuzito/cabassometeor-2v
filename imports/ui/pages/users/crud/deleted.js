/**
 * Created by lemux-dev on 27/01/17.
 */
import {remove} from '../../../../api/users/methods.js';
export class DeletedUsers{
    constructor(id){
        this.id = id;
        if(this.id != Meteor.user()._id){
            const remove_user= Meteor.call("users.delete",Meteor.user(),this.id, (err)=>{
                if(err){
                    swal("Error", "Hubo un error al borra", "error");
                }
                else {
                    swal({
                        title: "¡Usuario eliminado!",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "success"
                    });
                }
            });
        }else {
            swal("Error", "No se puede eliminar al usuario porque esta activo", "error");
        }

    }
}