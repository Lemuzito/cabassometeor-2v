/**
 * Created by alejandrolemusrodriguez on 20/01/17.
 */
import './created.html';


Template.userCreated.helpers({});
Template.userCreated.events({
    'submit #user-created-form': function (event, instace) {
        event.preventDefault();
        const form = event.target;
        const username = form.username.value;
        const email = form.email.value;
        const pass = form.password.value;
        const fullName = form.fullname.value;
        if (username && email && pass && fullName) {
            const data = {
                username: username, password: pass, email: email,
                profile: {fullName: fullName, email: email}
            };
            Meteor.call('users.insert', Meteor.user(), data, function(error){
                if(error.error == 'ok'){
                    $('#user-modal-create').modal('hide');
                    event.target.username.value = '';
                    event.target.email.value = '';
                    event.target.password.value = '';
                    event.target.fullname.value = '';
                    // Message of success
                    swal({
                        title: "¡Usuario agregado!",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "success"
                    });
                }else{
                    swal("Error", "Hubo un error al guardar", "error");
                }
            });
        }else{
            swal("Error", "Hubo un error al guardar", "error");
        }
    }
});